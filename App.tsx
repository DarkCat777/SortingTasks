import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import LoginScreen from "./views/authentication/login/Login";
import RegisterScreen from "./views/authentication/register/Register";
import ProfileScreen from "./views/authentication/profile/Profile";

const Stack = createStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login" screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name="Profile" component={ProfileScreen}
        />
        <Stack.Screen
          name="Login" component={LoginScreen}
        />
        <Stack.Screen
          name="Register" component={RegisterScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default App;
