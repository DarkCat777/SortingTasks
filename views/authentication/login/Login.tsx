import React from "react";

import { FirebaseAuthTypes } from "@react-native-firebase/auth";
// @ts-ignore
import { GoogleSignin } from "@react-native-google-signin/google-signin";

import { styles } from "./LoginStyles";

import {
  Text,
  ScrollView,
  ImageBackground,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity, Alert
} from "react-native";
import {
  configureGoogleSignIn,
  signInWithEmailAndPassword,
  signInWithGoogle
} from "../../service/AuthenticationService";

type LoginState = {
  user: FirebaseAuthTypes.User | null,
  email: string | null,
  password: string | null
}

export default class LoginScreen extends React.Component<any, LoginState> {

  constructor(props: any) {
    super(props);
    this.state = {
      user: null,
      email: null,
      password: null
    };
    configureGoogleSignIn();
  }

  authenticateWithEmailAndPassword() {
    const result = signInWithEmailAndPassword(this.state.email!, this.state.password!);
    if (result === null) {
      Alert.alert(
        "Error de credenciales",
        "Introduzca sus credenciales completas.",
        [
          {
            text: "Cancelar",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    } else {
      result!.then((user) => {
        this.props.navigation.navigate("Profile", { email: user.user.email, displayName: user.user.displayName });
      }).catch(error => {
        var message = "Su contraseña es incorrecta.";
        if (error.code === "auth/invalid-email") {
          message = "Su email es invalido.";
        }
        if (error.code === "auth/user-not-found") {
          message = "El usuario no existe registrese.";
        }
        Alert.alert(
          "Error de credenciales",
          message,
          [
            {
              text: "Cancelar",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => console.log("OK Pressed") }
          ]
        );
      });
    }
  }

  async onGoogleButtonPress() {
    signInWithGoogle().then((user) => {
      // Enviar a otra interfaz si la tuviera :v
      this.props.navigation.navigate("Profile", { email: user.user.email, displayName: user.user.displayName });
    }).catch(() => {
      Alert.alert(
        "Error de credenciales",
        "Algo Ocurrio",
        [
          {
            text: "Cancelar",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    });
  }

  render() {
    return <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffff" }}
      showsVerticalScrollIndicator={false}
    >
      <ImageBackground source={require("../../../images/fondo.png")}
                       style={{
                         height: Dimensions.get("window").height
                       }}>
        <View>
          <View style={styles.brandView}>
            <Image source={require("../../../images/icon_app.png")} />
          </View>
          <Text style={styles.brandViewText}>Iniciar Sesión</Text>
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/user.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Email"
              placeholderTextColor="#ffffff"
              onChangeText={(email) => this.setState({ email: email })}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/lock.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              secureTextEntry={true}
              style={styles.textInput}
              placeholder="Contraseña"
              placeholderTextColor="#ffffff"
              onChangeText={(password) => this.setState({ password: password })}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.RegisterSectionStyle}>
          <Text style={styles.brandViewPassword}>¿Olvidaste tu contraseña?</Text>
        </View>
        <View>
          <View style={styles.MainContainer}>
            <TouchableOpacity style={styles.buttonLogin} onPress={() => {
              this.authenticateWithEmailAndPassword();
            }}>
              <Text style={styles.TextStyle}> Iniciar Sesión</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.MainContainerDos}>
            <TouchableOpacity style={styles.GoogleStyle} onPress={() =>
              this.onGoogleButtonPress()
            }>
              <Image
                source={require("../../../images/google.png")}
                style={styles.ImageIconStyle}
              />
              <Text style={styles.TextStyleGoogle}> Iniciar Sesión con Google </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.RegisterSectionStyle}>
            <Text style={styles.brandViewRegister}>¿No tienes una cuenta?</Text>
            <Text style={styles.brandViewRegisterDos} onPress={() => {
              this.props.navigation.navigate("Register");
            }}>Registrate</Text>
          </View>
        </View>
      </ImageBackground>
    </ScrollView>;
  }
}
