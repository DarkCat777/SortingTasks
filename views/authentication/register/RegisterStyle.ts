import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    marginTop: 100,
    justifyContent: "center",
    alignItems: "center"
  },
  brandViewText: {
    marginTop: 80,
    marginLeft: 50,
    color: "#0FF2F8",
    fontSize: 32,
    fontWeight: "bold",
    fontFamily: "roboto"
  },
  brandViewRegister: {
    marginTop: 20,
    marginRight: 4,
    color: "#BDBDBD",
    fontSize: 15,
    fontFamily: "roboto"
  },
  brandViewPassword: {
    marginTop: 20,
    color: "#FFFFFF",
    fontSize: 15,
    fontFamily: "roboto",
    fontWeight: "400"
  },
  brandViewRegisterDos: {
    marginTop: 20,
    color: "#FFFFFF",
    fontSize: 15,
    fontFamily: "roboto",
    fontWeight: "bold"
  },
  textInput: {
    flex: 1,
    color: "#ffffff",
    fontSize: 15
  },
  containerInputs: {
    marginTop: 15
  },
  SectionStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 50,
    marginLeft: 50
  },
  RegisterSectionStyle: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 50,
    marginLeft: 50
  },
  hairline: {
    backgroundColor: "#fff",
    marginRight: 50,
    marginLeft: 50,
    height: 3
  },
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch",
    alignItems: "center"
  },
  MainContainer: {
    flex: 1,
    marginTop: 20,
    marginRight: 50,
    marginLeft: 50
  },
  MainContainerDos: {
    marginTop: 70,
    marginRight: 50,
    marginLeft: 50
  },
  buttonLogin: {
    backgroundColor: "#0FF2F8",
    flexDirection: "row",
    alignItems: "center",
    height: 50,
    borderRadius: 10,
    margin: 5,
    justifyContent: "center"
  },
  TextStyle: {
    color: "#FFFFFF",
    fontSize: 24,
    fontFamily: "roboto",
    fontStyle: "normal"
  },
  GoogleStyle: {
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 3,
    borderColor: "#0FF2F8",
    height: 50,
    borderRadius: 10,
    margin: 5,
    justifyContent: "center"
  },
  ImageIconStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch"

  },
  TextStyleGoogle: {
    color: "#0FF2F8",
    fontSize: 20
  },
  SeparatorLine: {
    backgroundColor: "#fff",
    width: 1,
    height: 40

  }
});
