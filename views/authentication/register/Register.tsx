import React from "react";
// @ts-ignore
import { GoogleSignin } from "@react-native-google-signin/google-signin";

import {
  Text,
  ScrollView,
  ImageBackground,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity, Alert
} from "react-native";
import { styles } from "./RegisterStyle";
import { registerAndSignInUser } from "../../service/AuthenticationService";

type RegisterState = {
  displayName: string | null,
  email: string | null,
  password: string | null,
  repeatPassword: string | null
}

export default class RegisterScreen extends React.Component<any, RegisterState> {


  render() {
    return <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffff" }}
      showsVerticalScrollIndicator={false}
    >
      <ImageBackground source={require("../../../images/fondo.png")}
                       style={{
                         height: Dimensions.get("window").height
                       }}>
        <View>
          <View style={styles.brandView}>
            <Image source={require("../../../images/icon_app.png")} />
          </View>
          <Text style={styles.brandViewText}>Registrate</Text>
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/user.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Usuario"
              placeholderTextColor="#ffffff"
              onChangeText={(displayName) => this.setState({ displayName: displayName })}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/mail.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Email"
              placeholderTextColor="#ffffff"
              onChangeText={(email) => this.setState({ email: email })}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/lock.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              secureTextEntry={true}
              style={styles.textInput}
              placeholder="Contraseña"
              placeholderTextColor="#ffffff"
              onChangeText={(password) => this.setState({ password: password })}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/lock.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              secureTextEntry={true}
              style={styles.textInput}
              placeholder="Repetir Contraseña"
              placeholderTextColor="#ffffff"
              onChangeText={(password) => this.setState({ repeatPassword: password })}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View>
          <View style={styles.MainContainer}>
            <TouchableOpacity style={styles.buttonLogin} onPress={() => {
              this.registerNewUser();
            }}>
              <Text style={styles.TextStyle}> Registrate</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </ScrollView>;
  }

  private registerNewUser() {
    if (this.state.password == this.state.repeatPassword) {
      registerAndSignInUser(this.state.email!, this.state.password!, this.state.displayName!, null!)
        .then(() => {
          this.props.navigation.navigate("Profile");
        })
        .catch(() => {
          Alert.alert(
            "Error en registro",
            "Algo salio mal al registrarse",
            [
              {
                text: "Cancelar",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
          );
        });
    }
  }
}

