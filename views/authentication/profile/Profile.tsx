import React from "react";

import auth, { FirebaseAuthTypes } from "@react-native-firebase/auth";
// @ts-ignore
import { GoogleSignin } from "@react-native-google-signin/google-signin";

import {
  Text,
  ScrollView,
  ImageBackground,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity, Alert
} from "react-native";
import { styles } from "./ProfileStyles";
import { isGoogleAccount, signOut, updateUserAuth } from "../../service/AuthenticationService";

type ProfileState = {
  user: FirebaseAuthTypes.User | null,
  email: string | null,
  oldPassword: string | null,
  newPassword: string | null,
  displayName: string | null,
  isGoogleAccount: boolean
}

export default class ProfileScreen extends React.Component<any, ProfileState> {

  constructor(props: any) {
    super(props);
    this.state = {
      user: auth().currentUser!,
      email: auth().currentUser!.email,
      oldPassword: "",
      newPassword: "",
      displayName: auth().currentUser!.displayName,
      isGoogleAccount: !isGoogleAccount()
    };
  }

  render() {
    return <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffff" }}
      showsVerticalScrollIndicator={false}
    >
      <ImageBackground source={require("../../../images/fondodos.png")}
                       style={{
                         height: Dimensions.get("window").height
                       }}>
        <View>
          <View style={styles.brandView}>
            <Image source={require("../../../images/profilepicture.png")} />
          </View>
          <Text style={styles.brandViewText}>Mi Información</Text>
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/user.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Usuario"
              placeholderTextColor="#ffffff"
              value={this.state.displayName!}
              onChangeText={(displayName) => this.setState({ displayName: displayName })}
              editable={this.state.isGoogleAccount}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/mail.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Email"
              placeholderTextColor="#ffffff"
              value={this.state.email!}
              onChangeText={(email) => this.setState({ email: email })}
              editable={this.state.isGoogleAccount}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/lock.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              secureTextEntry={true}
              style={styles.textInput}
              placeholder="Contraseña antigua"
              placeholderTextColor="#ffffff"
              onChangeText={(oldPassword) => this.setState({ oldPassword: oldPassword })}
              editable={this.state.isGoogleAccount}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View style={styles.containerInputs}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../../../images/lock.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              secureTextEntry={true}
              style={styles.textInput}
              placeholder="Contraseña nueva"
              placeholderTextColor="#ffffff"
              onChangeText={(newPassword) => this.setState({ newPassword: newPassword })}
              editable={this.state.isGoogleAccount}
            />
          </View>
          <View style={styles.hairline} />
        </View>
        <View>
          <View style={styles.MainContainer}>
            <TouchableOpacity style={styles.buttonLogin} onPress={() => {
              // Funcion cuando se tenga una cuenta registrada en firebase
              this.updateUserInformation();
            }} disabled={!this.state.isGoogleAccount}>
              <Text style={styles.TextStyle}> Actualizar Información</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.MainContainerDos}>
            <TouchableOpacity style={styles.GoogleStyle} onPress={() =>
              this.onPressSignOutButton()
            }>
              <Text style={styles.TextStyleGoogle}> Cerrar Sesión </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </ScrollView>;
  }

  private onPressSignOutButton() {
    // @ts-ignore
    signOut().then(() => {
      this.props.navigation.navigate("Login");
    });
  }

  private updateUserInformation() {
    const { oldPassword, email, newPassword, displayName } = this.state;
    const result = updateUserAuth(oldPassword!, email!, newPassword!, displayName!, null!);
    if (result != null) {
      this.setState({
        user: auth().currentUser!,
        email: auth().currentUser!.email,
        displayName: auth().currentUser!.displayName,
        isGoogleAccount: !isGoogleAccount()
      });
      Alert.alert(
        "Datos Actualizados",
        "Actualización de perfil exitosa.",
        [
          {
            text: "Cancelar",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    } else {
      Alert.alert(
        "Error al actualizar lo datos del usuario",
        "Algo ocurrio :(",
        [
          {
            text: "Cancelar",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    }
  }
}
