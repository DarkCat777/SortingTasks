import { GoogleSignin } from "@react-native-google-signin/google-signin";
import auth from "@react-native-firebase/auth";

export function configureGoogleSignIn() {
  GoogleSignin.configure({
    webClientId: "626718208802-12kkbcp8s19h29u1f5ntn08qa3sl1gtn.apps.googleusercontent.com"
  });
}

export function signInWithEmailAndPassword(email: string, password: string) {
  if (email != null && password != null) {
    return auth().signInWithEmailAndPassword(email!, password!);
  } else {
    return null;
  }
}

export async function signInWithGoogle() {
  const { idToken } = await GoogleSignin.signIn();
  // Create a Google credential with the token
  const googleCredential = auth.GoogleAuthProvider.credential(idToken);
  // Sign-in the user with the credential
  return await auth().signInWithCredential(googleCredential);
}

export function getCurrentAccount() {
  return auth().currentUser;
}

export function updateUserAuth(oldPassword: string, email: string, newPassword: string, displayName: string, photoURL: string) {
  if (oldPassword != null || email != null || newPassword != null || displayName != null || photoURL != null) {
    return getCurrentAccount()!
      .reauthenticateWithCredential(auth.EmailAuthProvider.credential(getCurrentAccount()!.email!, oldPassword))
      .then(() => {
        if (newPassword != null)
          getCurrentAccount()!.updatePassword(newPassword);
        if (email != null && email != getCurrentAccount()?.email)
          getCurrentAccount()!.updateEmail(email);
        if (displayName != null)
          getCurrentAccount()!.updateProfile({ displayName: displayName });
        if (photoURL != null) {
          getCurrentAccount()!.updateProfile({ photoURL: photoURL });
        }
      });
  }
  return null;
}

export function registerAndSignInUser(email: string, password: string, displayName: string, photoURL: string) {
  return auth().createUserWithEmailAndPassword(email, password).then((user) => {
    return getCurrentAccount()!
      .reauthenticateWithCredential(auth.EmailAuthProvider.credential(email, password)).then(() => {
        if (displayName != null)
          getCurrentAccount()!.updateProfile({ displayName: displayName });
        if (photoURL != null) {
          getCurrentAccount()!.updateProfile({ photoURL: photoURL });
        }
      });
  });
}

export function isGoogleAccount() {
  return auth().currentUser?.providerData[0].providerId === "google.com";
}

export function signOut() {
  if (isGoogleAccount()) {
    return GoogleSignin.signOut();
  } else {
    return auth().signOut();
  }
}
